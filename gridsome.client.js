import {
    FontAwesomeIcon,
    FontAwesomeLayersText,
    FontAwesomeLayers
  } from "@fortawesome/vue-fontawesome";
  import { library } from "@fortawesome/fontawesome-svg-core";
  import {
    faTwitter,
    faTumblrSquare,
    faInstagram,
    faFacebook,
    faEtsy
  } from "@fortawesome/free-brands-svg-icons";
  import {
    faFileAlt,
    faEnvelope,
    faBars,
    faThList,
    faInfo,
    faAlignLeft,
    faHeart,
    faQuoteLeft,
    faQuoteRight,
    faUserFriends,
    faStore,
    faShoppingBag,
    faShoppingBasket,
    faShoppingCart,
    faSearch,
    faLocationArrow,
    faPhone
  } from "@fortawesome/free-solid-svg-icons";
  
  library.add(
    faTwitter,
    faFileAlt,
    faEnvelope,
    faBars,
    faThList,
    faInfo,
    faAlignLeft,
    faHeart,
    faQuoteLeft,
    faQuoteRight,
    faTumblrSquare,
    faInstagram,
    faFacebook,
    faUserFriends,
    faStore,
    faShoppingBag,
    faShoppingBasket,
    faShoppingCart,
    faEtsy,
    faSearch,
    faLocationArrow,
    faPhone
  );
  
  export default function(Vue) {
    // Vue.component("font-awesome", FontAwesomeIcon);
    Vue.component("font-awesome-icon", FontAwesomeIcon);
    Vue.component("font-awesome-layers-text", FontAwesomeLayersText);
    Vue.component("font-awesome-layers", FontAwesomeLayers);
  }
  